#!/usr/bin/env python

"""
Code for Population Dynamics (W.Kinzel/G.Reents, Physics by Computer)

This code is based on logmap.m listed in Appendix E of the book and will
replicate Figs. 3.1-3.7 of the book.
"""

__author__ = "Christian Alis"
__credits__ = "W.Kinzel/G.Reents"

#==============================================================================
# NOTE: The comments enclosed in ## are my (J.L. Euste) comments/answers
#==============================================================================

import numpy as np
import matplotlib.pyplot as plt
from array import array

f = lambda x, r: 4 * r * x * (1-x)

def iterf(f, x0, r, n):
    '''
    Iterate a function.
    
    Parameters
    ----------
    f : function
	    Function for iteration.
    x0 : float
	    Initial value for iteration.
    r : float
	    Parameter constant in f.
    n : int
         Number of iterations.
    '''

    # Is it possible to vectorize this function? Explain why or why not.
        ##No. Each element (except the initial value) depends on the previous element##

    out = [x0]
    for _ in xrange(n):
        out.append(f(out[-1], r))
    return out

def plot_3_1(f=f, x0=0.65, r=0.87, maxiter=40):
    '''
				
    Plot the value of x after n iterations.
	
    Parameters
    ----------
    f : function
	    Function for iteration.
    x0 : float
	    Initial value for iteration.
    r : float
	    Parameter constant in f.
    maxiter : int
	    Maximum nuber of iteration.
    '''
    ns = xrange(maxiter+1)
    xs = iterf(f, x0, r, maxiter)
    plt.plot(ns, xs, '--o')
    plt.xlim(0, maxiter+1)
    plt.xlabel('$n$',size='large')
    plt.ylabel('$x(n)$',size='large')
    plt.show()
    
def plot_3_2(f=f, r=0.87):
    '''
    Plot the x,f(x) and its two-fold and four-fold iterations.
    
    Parameters
    ----------
    f : function
        Function for iteration.
    r : float
        Parameter constant in f.

    '''
    x = np.linspace(0, 1, 500)
    fx1 = f(x, r)
    fx2 = f(fx1, r)
    # complete the following two lines
        ##done##
    fx3 = f(fx2,r)
    fx4 = f(fx3,r)
    plt.plot(x, x, 'k',label='$x$')
    plt.plot(x, fx1, '--', color='gray',label='$f(x)$')
    # What is the python type of the value of color?
        ##color can have a value that is a string of 3 or 4 element sequence##
    # What would happen if (0.6)*3 is used instead of (0.6,)*3?
        ##error because color takes a sequence with 3 elements for the RGB values...
        ##...whereas (0.6)*3 is a float##
    # What would happen if (0.3,)*3 is used instead of (0.6,)*3?
        ##no change##
    # What would happen if (0.9,)*3 is used instead of (0.6,)*3?
	  ##no change##
    plt.plot(x, fx2, color=(0.9,)*3,label='$f^{(2)}(x)$')
    plt.plot(x, fx4, 'k',label='$f^{(4)}(x)$')
    plt.legend(loc=0)
    plt.xlabel('$x$',size='large')
    plt.ylabel('$f^{(n)}(x)$',size='large')
    plt.show()
    
def plot_3_3(x0=0.3, r_min=0, r_max=1):
    '''
    Make a Feigenbaum plot.
    
    Parameters
    ----------
    x0 : float
        Initial value for iteration.
    r_min : int or float
        Minimum value of the parameter constant
    r_max : int or float
        Maximum value of the parameter constant.
    '''
    rs = np.linspace(r_min, r_max, 500)
    # What is the purpose of multiplying the output of np.ones() and a number?
        ##this makes an array full of the multiplied number with the same...
        ##.length as np.ones()##
    # What are the dimensions of fxs?
        ##1001x500###
    # What do the rows and columns of fxs represent?
        ##the nth row represents the value of x_n while each column represents...
        ##...each value of r in the the array rs##
    fxs = np.array(iterf(f, np.ones(len(rs))*0.3, rs, 1000))
    # What are the dimensions of fxs?
        ##1001x500##
    # What does .T in the second argument do?
        ##transpose the array##
    # Can we use fxs[100:,:] instead of fxs[100:,:].T? Why or why not?
        ##No! fxs[100:,:].T doesn't have the same dimension as rs##
    # What does the keyword ms do?
        ##changes the marker size##
    plt.plot(rs, fxs[100:,:].T, 'k.', ms=2)
    plt.xlabel('$r$',size='large')
    plt.ylabel('$f(x)$',size='large')
    plt.show()
    
def plot_3_4(x0=0.3):
    '''
    Make a close-up of the Feigenbaum plot from r=0.88 to r=1.
    
    Parameters
    ----------
    x0 : float
        Initial value for iteration.
    r_min : int or float
        Minimum value of the parameter constant
    r_max : int or float
        Maximum value of the parameter constant.
    '''
    plot_3_3(r_min=0.88)
    
def plot_3_5(x0=0.3, r = 0.934):
    '''
    Plot the frequency distribution of x_n.
    
    Parameters
    ----------
    x0 : float
	    Initial value for iteration.
    r : float
	    Parameter constant in f.
    
    '''
    
    # Complete the following line
        ##done##
    fxs = iterf(f, x0, r, 400)
    plt.hist(fxs, bins=100)
    plt.xlim(xmin=0)
    plt.xlabel('$n$',size='large')
    plt.ylabel('frequency',size='large')
    plt.show()
    
def plot_3_6(x0=0.3, r=0.874640, maxiter=1000):
    '''
    Plot the superstable four-cycle.
    
    Parameters
    ----------
    f : function
	    Function for iteration.
    x0 : float
	    Initial value for iteration.
    r : float
	    Parameter constant in f.
    maxiter : int
	    Maximum nuber of iteration.
    '''
    xs = np.linspace(0, 1)
    fxs = iterf(f, x0, r, 1000)[100:]
    # complete the next two lines
        ##done##
    plt.plot(xs, xs)
    plt.plot(xs, f(xs,r))
    # What are the first 10 elements of the first argument of the ff. 
    # plt.plot() call?
        ##the first 5 elements of fxs since np.repeat() was used##
    # Why was the last element of fxs omitted in the argument of np.repeat()?
        ##because the last element is the last element of the iteration. it does not have a...
        ##corresponding x_n since it is the last f(x_n)##
    # Can we use fxs[:-1]*2 instead of np.repeat(fxs[:-1], 2)? Why or why not?
        ##No! fxs[:-1]*2 will repeat the entire list after the list while...
        ##...np.repeat(fxs[:-1], 2) repeat evey element of fxs[:-1] after each element##
    plt.plot(np.repeat(fxs[:-1], 2), 
    # What does the following argument of plt.plot() call represent?
        ##it draws the Cobweb plot for the logistic map##
    # What are the dimensions/shape of the following plt.plot() argument?
        ##1800##
    # What are the dimensions of the argument of np.column_stack()?
        ##900 x 2##
    # Why was the first element of fxs[1:] omitted?
        ##Because it contains the intial value of iteration x_0##
    # What would happen if .ravel() is removed?
        ##There will be an  error since np.column_stack((fxs[:-1], fxs[1:]))...
        ##... does not the same dimension as np.ravel(zip(fxs[:,1], fxs[1:]))##
    # Can we use np.ravel(zip(fxs[:,1], fxs[1:])) instead of 
    # np.column_stack((fxs[:-1], fxs[1:])).ravel()? Why or why not?
        ##Yes! (there might be a typo in the question, it should be np.ravel(zip(fxs[:-1], fxs[1:]))...
        ##...instead of np.ravel(zip(fxs[:,1], fxs[1:]))) This is because np.column_stack()...
        ##...does the same thing as zip() except that the former returns an array while the latter...
        ##...returns a list. That array or list is then ravelled##
             np.column_stack((fxs[:-1], fxs[1:])).ravel(), 
             '-o')
    plt.xlabel('$x$',size='large')
    plt.ylabel('$y$',size='large')
    plt.show()
    
def plot_3_7(x0=0.3, r=0.96420, maxiter=1000):
    '''
    Plot the superstable four-cycle.
    
    Parameters
    ----------
    f : function
	    Function for iteration.
    x0 : float
	    Initial value for iteration.
    r : float
	    Parameter constant in f.
    maxiter : int
	    Maximum nuber of iteration.
    '''
    plot_3_6(x0,r,maxiter)
    
# OPTIONAL:
# * Replace np.ones(len(rs))*0.3 with an equivalent expression that uses
#   the Python standard library only (no numpy)
    ##use list comprehension and the array function from the array module in the...
    ##... Python standard library##
    ##replace np.ones(len(rs))*0.3 with array([0.3 for _ in xrange(len(rs))])##
# * write iterf() as a recursive function
    ##iterf_recursive() is a recursive function that has the same output as iterf()...
    ##... but we need to input the list in which the values of x_n are stored##
    f = lambda x, r: 4 * r * x * (1-x)
    def iterf_recursive(f, x0, r, n,i,out):
        '''
        Iterate a function.
        
        Parameters
        ----------
        f : function
        	   Function for iteration.
        x0 : float
        	   Initial value for iteration.
        r : float
            Parameter constant in f.
        n : int
             Number of iterations.
        i : inter
            Counter to keep track the number of iterations done.
        out : list
            List to store the values of x_n
        '''       
        if i < n:
            out.append(f(x0,r))
            iterf(f,out[-1],r,n,i+1,out)
        return out    
# * estimate the Feigenbaum constant
# * add interactivity to your plots

if __name__ == "__main__":
    plot_3_1()
    plot_3_2()
    plot_3_3()
    plot_3_4()
    plot_3_5()
    plot_3_6()
    plot_3_7()